class Timer
  attr_accessor :seconds
  def initialize
    @seconds = 0
  end

  def time_string
    secs, mins, hours = @seconds % 60, (@seconds / 60) % 60, @seconds / 60 / 60
    "#{padded(hours)}:#{padded(mins)}:#{padded(secs)}"
  end

  def padded(i)
    "0#{i}"[-2, 2]
  end
end
