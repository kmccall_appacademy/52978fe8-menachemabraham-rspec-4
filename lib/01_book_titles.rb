class Book
  FILLERS = %w(a the an and in of to)

  attr_reader :title
  def title=(name)
    words = name.capitalize.split
    @title = words.map { |word| title_case(word) }.join(" ")
  end

  private

  def title_case(word)
    FILLERS.include?(word) ? word : word.capitalize
  end
end
