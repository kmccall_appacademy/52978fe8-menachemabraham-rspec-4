class Dictionary
  attr_accessor :entries

  def initialize(entries = {})
    @entries = entries
  end

  def add(entries)
    hashed = entries.is_a?(Hash) ? entries : { entries => nil }
    @entries.merge!(hashed)
  end

  def keywords
    @entries.keys.sort
  end

  def include?(key)
    @entries.include?(key)
  end

  def find(key)
    if @entries.key?(key)
      { key => @entries[key] }
    else
      @entries.select { |k, _| k.index(key) == 0 }
    end
  end

  def printable
    @entries.sort.map { |k, v| "[#{k}] \"#{v}\"" }.join("\n")
  end
end
