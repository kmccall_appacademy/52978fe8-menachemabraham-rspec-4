# MyHashSet
#
# Ruby provides a class named `Set`. A set is an unordered collection of
# values with no duplicates.  You can read all about it in the documentation:
#
# http://www.ruby-doc.org/stdlib-2.1.2/libdoc/set/rdoc/Set.html
#
# Let's write a class named `MyHashSet` that will implement some of the
# functionality of a set. Our `MyHashSet` class will utilize a Ruby hash to keep
# track of which elements are in the set.  Feel free to use any of the Ruby
# `Hash` methods within your `MyHashSet` methods.
#
# Write a `MyHashSet#initialize` method which sets an empty hash object to
# `@store`. Next, write an `#insert(el)` method that stores `el` as a key
# in `@store`, storing `true` as the value. Write an `#include?(el)`
# method that sees if `el` has previously been `insert`ed by checking the
# `@store`; return `true` or `false`.
#
# Next, write a `#delete(el)` method to remove an item from the set.
# Return `true` if the item had been in the set, else return `false`.  Add
# a method `#to_a` which returns an array of the items in the set.
#
# Next, write a method `set1#union(other)` which returns a new set which
# includes all the elements in `set1` or `other` (or both). Write a
# `set1#intersect(other)` method that returns a new set which includes only
# those elements that are in both `set1` and `other`.
#
# Write a `set1#minus(other)` method which returns a new set which includes
# all the items of `set1` that aren't in `other`.

class MyHashSet
  def initialize
    @store = Hash.new(false)
  end

  def insert(el)
    @store[el] = true
  end

  def include?(el)
    @store[el]
  end

  def delete(el)
    @store.delete(el)
  end

  def to_a
    @store.keys
  end

  def union(other)
    result = dup
    other.to_a.each { |el| result.insert(el) }
    result
  end

  def intersect(other)
    result = self.class.new
    to_a.each { |el| result.insert(el) if other.include?(el) }
    result
  end

  def minus(other)
    result = dup
    other.to_a.each { |el| result.delete(el) }
    result
  end

  def symmetric_difference(other)
    result = union(other)
    result.to_a.each { |el| result.delete(el) if include?(el) && other.include?(el) }
    result
  end

  def ==(other)
    to_a == other.to_a
  end
end

# Bonus
#
# - Write a `set1#symmetric_difference(other)` method; it should return the
#   elements contained in either `set1` or `other`, but not both!
# - Write a `set1#==(object)` method. It should return true if `object` is
#   a `MyHashSet`, has the same size as `set1`, and every member of
#   `object` is a member of `set1`.
