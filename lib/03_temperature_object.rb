class Temperature
  attr_reader :in_celsius, :in_fahrenheit

  def self.ftoc(fahrenheit)
    (fahrenheit - 32) * (5.0 / 9.0)
  end

  def self.ctof(celsius)
    celsius * (9.0 / 5.0) + 32
  end

  def self.from_fahrenheit(degrees)
    new(f: degrees)
  end

  def self.from_celsius(degrees)
    new(c: degrees)
  end

  def initialize(options = {})
    @in_fahrenheit = options[:f] || self.class.ctof(options[:c])
    @in_celsius = options[:c] || self.class.ftoc(@in_fahrenheit)
  end
end

class Celsius < Temperature
  def self.new(degrees)
    Temperature.from_celsius(degrees)
  end
end

class Fahrenheit < Temperature
  def self.new(degrees)
    Temperature.from_fahrenheit(degrees)
  end
end
